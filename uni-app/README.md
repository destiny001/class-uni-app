# Headline

::: tip
基于 uniapp 开发的微信小程序商城项目
:::

https://www.escook.cn/docs-uni-shop/

## tabBar 相关的页面

### 首页

![w317](./images/0-1.png)

### 分类

![w317](./images/0-2.png)

### 购物车

![w317](./images/0-3.png)
![w317](./images/0-4.png)
![w317](./images/0-5.png)

### 我的

![w317](./images/0-6.png)
![w317](./images/0-7.png)
![w317](./images/0-8.png)

## 搜索

![w317](./images/0-9.png)
![w317](./images/0-10.png)

## 商品列表

![w317](./images/0-14.png)

## 商品详情

![w317](./images/0-11.png)
![w317](./images/0-12.png)
![w317](./images/0-13.png)

## 微信支付

![w317](./images/0-15.png)